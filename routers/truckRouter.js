const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./asyncWrapper');
const {validateToken} = require('./middlewares/validateTokenMiddleware');
const {validateTruck} = require('./middlewares/validateTruckMiddleware');
const {getRelatedTrucks} = require('./middlewares/getRelatedTrucksMiddleware');
const {
  checkIfUserIsFree,
} = require('./middlewares/checkIfUserIsFreeMiddleware');
const {
  checkIfUserIsDriver,
  // checkIfUserIsShipper,
} = require('./middlewares/checkUserRoleMiddleware');
const {
  addTruck,
  getTrucks,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,

} = require('../controllers/truckControllers.js');


router.get('/',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsDriver),
    asyncWrapper(getTrucks),
);

router.post('/',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsDriver),
    asyncWrapper(validateTruck),
    asyncWrapper(addTruck),
);
router.get('/:id',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsDriver),
    asyncWrapper(getRelatedTrucks),
    asyncWrapper(getTruckById),
);
router.put('/:id',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsDriver),
    asyncWrapper(checkIfUserIsFree),
    asyncWrapper(getRelatedTrucks),
    asyncWrapper(validateTruck),
    asyncWrapper(updateTruckById),
);
router.delete('/:id',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsDriver),
    asyncWrapper(checkIfUserIsFree),
    asyncWrapper(getRelatedTrucks),
    asyncWrapper(deleteTruckById),
);
router.post('/:id/assign',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsDriver),
    asyncWrapper(checkIfUserIsFree),
    asyncWrapper(getRelatedTrucks),
    asyncWrapper(assignTruckById),
);

module.exports = router;
