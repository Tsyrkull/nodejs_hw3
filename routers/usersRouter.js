const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./asyncWrapper');
const {validateToken} = require('./middlewares/validateTokenMiddleware');
const {
  validatePasswordChange,
} = require('./middlewares/validatePasswordChangeMiddleware');
const {checkIfUserIsShipper} = require('./middlewares/checkUserRoleMiddleware');
const {
  checkIfUserIsFree,
} = require('./middlewares/checkIfUserIsFreeMiddleware');


const {
  getUserProfileInfo,
  deleteUser,
  changeUserPassword,
} = require('../controllers/userControllers');

router.get('/me',
    asyncWrapper(validateToken),
    asyncWrapper(getUserProfileInfo));

router.delete('/me',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsShipper),
    asyncWrapper(deleteUser));

router.patch('/me/password',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsFree),
    asyncWrapper(validatePasswordChange),
    asyncWrapper(changeUserPassword));

module.exports = router;
