const {Truck} = require('../models/truckModel');
const {truckTypes} = require('../helpers/truckTypes');
const {BadRequestError} = require('../errors');

const addTruck = async (req, res, next) => {
  const type = req.body.type;
  const createdBy = req.user._id;
  const {length, width, height, payload} = truckTypes[type];

  const truck = new Truck({
    created_by: createdBy,
    type,
    length,
    width,
    height,
    payload,
  });

  await truck.save();
  res.status(200).json({message: 'Truck was created successfully'});
};

const getTrucks = async (req, res, next) => {
  const trucks = await Truck.find(
      {created_by: req.user._id},
      {__v: 0});

  res.status(200).json({trucks: trucks});
};

const getTruckById = async (req, res, next) => {
  res.status(200).json({truck: req.truck});
};

const updateTruckById = async (req, res, next) => {
  if (req.truck.assigned_to) {
    throw new BadRequestError('You cant update assigned truck');
  }

  const type = req.body.type;
  const {length, width, height, payload} = truckTypes[type];
  await Truck.updateOne(req.truck, {
    type: type,
    length: length,
    width: width,
    height: height,
    payload: payload,
  });

  res.status(200).json({message: 'Your truck was updated successfully'});
};

const deleteTruckById = async (req, res, next) => {
  if (req.truck.assigned_to) {
    throw new BadRequestError('You cant delete assigned truck');
  }
  await Truck.deleteOne(req.truck);

  res.status(200).json({message: 'Truck deleted successfully'});
};

const assignTruckById = async (req, res, next) => {
  await Truck.findOneAndUpdate({assigned_to: req.user._id}, {
    $set: {
      assigned_to: null,
    },
  });

  await Truck.findOneAndUpdate({_id: req.truck._id}, {
    $set: {
      assigned_to: req.user._id,
    },
  });

  res.status(200).json({message: 'Truck assigned successfully'});
};

module.exports = {
  addTruck,
  getTrucks,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
