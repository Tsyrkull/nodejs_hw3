require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const morgan = require('morgan');


const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/usersRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

const {BadRequestError} = require('./errors');

const port = process.env.PORT || 8080;


app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter );
app.use('/api/trucks', truckRouter );
app.use('/api/loads', loadRouter );


app.use((err, req, res, next) => {
  if (err instanceof BadRequestError) {
    return res.status(err.statusCode).json({message: `${err.message}`});
  }
  return res.status(500).json({message: err.message});
});


const start = async () => {
  await mongoose.connect('mongodb+srv://atlasuser:atlasuser@cluster0.xl5kv.mongodb.net/hw3?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Server for HW3 is working on port ${port}`);
  });
};

start();
