# NodeJS HomeWork3

The very last Homework for our first NodeJS Module.

## Installation

You need [nodeJS](https://nodejs.org/) installed on your computer to use HW3 project.

To install all dependencies run:

```bash
npm install
```
To start server run:

```bash
npm start
```
